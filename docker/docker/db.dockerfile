FROM mysql:5.7

COPY docker/docker/userweb_db_start.sh /home
COPY docker/docker/db.sql /home

RUN chmod 755 /home/userweb_db_start.sh