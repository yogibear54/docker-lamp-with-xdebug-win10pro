FROM php:7.0-apache

COPY httproot /var/www/html
COPY docker/docker/userweb_start.sh /home
COPY docker/docker/vhosts.conf /etc/apache2/sites-available/000-default.conf
COPY docker/docker/php.ini /usr/local/etc/php/
 
WORKDIR /var/www/html
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install vim -y --allow-unauthenticated
RUN pecl install xdebug
RUN docker-php-ext-install pdo pdo_mysql mysqli
RUN a2enmod rewrite filter
RUN chown www-data:www-data /tmp/xdebug.log
RUN chmod 755 /home/userweb_start.sh
RUN service apache2 restart