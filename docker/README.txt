
First run:
docker-compose up

-------------------------------------------------

MIGRATIONS AND CONTAINER EXECUTABLE AFTER STARTUP
This is for running any executables (i.e. migrations
database imports, etc) after your container has 
started.

Run Bash:
docker exec -it [name of container] bash -c docker/start.sh