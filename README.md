# Docker for Windows 10 with xDebug (LAMP installation)
Date: August 7th 2018

This is a setup that we've used recently with our Codeigniter setup.

It took a lot of blood, sweat and tears, going through a lot of blogs stackoverflow, reddit...

This setup is done with:

- Windows 10 Pro (Version 10.0.17134)
- Docker version 18.06.0-ce
- docker-compose version 1.22.0, build f46880fe

This setup has worked for our team.

Pay attention to our folder structure, as this setup is for this structure.

## Things to pay attention for:
In docker-compose.yml:

- In web > build > context - since httproot is in a parent folder, we need to build from the parent context.
- In web > volumes - although context is set with "..", we still need to include ".." for volumes, not exactly sure why.

In vscode, launch.json file (you can find a sample in the setting-up-docker.docx):
This is the configuration file for PHP Debug.

- Need to remember to include pathMappings in the format:
  - [webroot within the server]: [webroot within your local using windows folder convention]

In your Windows 10 WSL, your C drive (the folder where files are shared), needs to be bound to root:

- There is a sample wsl.conf file, which can be used with Windows Build 17093 and later, which will do what you need
OR
- follow instructions in the doc for alternative method

# Some helpful stuff:
When setting up in the beginning, I found it useful having the "Docker" extension (https://marketplace.visualstudio.com/items?itemName=PeterJausovec.vscode-docker) installed.  It allowed me to easily clear containers and remove images when necessary. 

